--Teradata

-- 1) Dada una tabla listar sus columnas

select
  columnname,
  columntype,
  columnlength,
  DecimalTotalDigits,
  DecimalFractionalDigits,
  nullable
from dbc.columns                     Col
where Col.TableName like 'DBMSMetadata_Prueba%'

-- 2) Listar tablas que cumplan una condici�n

select TableName
from dbc.tables
where tablename like 'DBMSMetadata_Prueba%'


select *
from dbc.tables
where tablename like 'DBMSMetadata_Prueba%'

-- 3) Por el momento incapaz de mostrar tama�o de la tabla

select
  tablesize.databasename,
  tablesize.tablename,
  sum(currentperm)
from dbc.tablesize tablesize
where tablesize.tablename like 'DBMSMetadata_Prueba%'
group by tablesize.databasename, tablesize.tablename

-- 4) Constraints

(
  select
     TableName,
     IndexName,
     case
       when IndexType = 'K' then 'PRIMARY KEY'
       when IndexType = 'U' then 'UNIQUE'
       else IndexType
     end as IndexType
  from dbc.Indices
  where IndexType <> 'S'
    and IndexName is not null
    and upper(TableName) like upper('dbmsmetadata_prueba%')
)
union
(
  select
    ChildTable,
    IndexName,
    'FOREIGN KEY' as IndexType
  from dbc.ALL_RI_Children
  where upper(ChildTable) like upper('dbmsmetadata_prueba%')
)

-- 5) Indices

select
  TableName,
  IndexName
from dbc.Indices
where upper(TableName) like upper('dbmsmetadata_prueba%')
  and IndexName is not null --Si la tabla no tiene constraints aparece a nulo

-- 6)

SELECT InfoData
FROM DBC.DBCInfo
where InfoKey = 'VERSION';





